﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDriverTask.HelpClass
{
    public static class LetterContentGenerator
    {
        public static string GeneratorRandomEmailContent(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789 ";
            StringBuilder contenBuilder = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                char randomChar = chars[random.Next(chars.Length)];
                contenBuilder.Append(randomChar);
            }
            return contenBuilder.ToString();
        }


        public static string GeneratorNickName(int length)
        {
            Random random = new Random();
            const string charBox = "abcdefghijklmnopqrstuvwxyz";
            int randomIndex = random.Next(0, charBox.Length);
            char firstChar = Char.ToUpper(charBox[randomIndex]);
            StringBuilder contenBuilder = new StringBuilder();
            contenBuilder.Append(firstChar);
            for (int i = 0; i < length - 1; i++)
            {
                char randomChar = charBox[random.Next(charBox.Length)];
                contenBuilder.Append(randomChar);
            }

            return contenBuilder.ToString();
        }

    }
}

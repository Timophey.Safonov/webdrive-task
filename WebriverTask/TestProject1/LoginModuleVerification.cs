using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;

namespace SeleniumCsharp

{

    public class LogIn

    {
        readonly string testUrl = "https://proton.me/";
        IWebDriver driver;


        [OneTimeSetUp]

        public void Setup()

        {
            driver = new ChromeDriver(@"D:\Studing\SELF-EDUCATION\AQA EPAM\C#\Test Automation\webdrive-task\WebriverTask\TestProject1\driver\chromedriver-win64\chromedriver-win64");
        }

        /// <summary>
        /// Verifies the login process with valid email credentials.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Sets an implicit wait timeout.
        /// 2. Navigates to the specified test URL.
        /// 3. Locates the sign-in element and clicks it.
        /// 4. Enters a test email and password.
        /// 5. Clicks the login button.
        /// 6. Verifies the presence of a logo element.
        /// 7. Opens the user menu and logs out.
        /// </remarks>
        [Test]
        public void VerifyEmailWIthValidCredentials()

        {

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            driver.Url = testUrl;
            IWebElement signInElement = driver.FindElement(By.CssSelector("#gatsby-focus-wrapper > div.fixed.top-0.z-30.w-full.duration-75.ease-linear.bg-purple-25 > div > div > header > div > div.ml-auto.hidden.items-center.xl\\:shrink-0.md\\:flex.md\\:gap-4 > a.flex.flex-row.items-center.text-center.font-bold.rounded-full.group.transition-all.duration-300.no-underline.justify-center.items-center.transition-all.px-3.py-1\\.5.text-sm.text-purple-500.hover\\:\\!bg-purple-500.hover\\:\\!text-white.button-hover-shadow.focus\\:bg-purple-500.focus\\:text-white.w-fit > span > span"));

            signInElement.Click();

            IWebElement emailElement =
                driver.FindElement(By.CssSelector("#username"));
            emailElement.SendKeys("testEmailWebDriver@proton.me");
            IWebElement password = driver.FindElement(By.XPath("//*[@id=\"password\"]"));
            password.SendKeys("J3kCetx!6gvrdB:");
            IWebElement loginBtn =
                driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div[1]/main/div[1]/div[2]/form/button"));
            loginBtn.Click();

            IWebElement logo = driver.FindElement(By.CssSelector(
                "body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div.content.ui-prominent.flex-item-fluid-auto.flex.flex-column.flex-nowrap.reset4print > div > div.sidebar.flex.flex-nowrap.flex-column.no-print.outline-none > div.logo-container.flex.flex-justify-space-between.flex-align-items-center.flex-nowrap.no-mobile > a > svg"));

            Assert.IsTrue(logo.Displayed);

            driver.FindElement(By.CssSelector(
                "body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div > div > div.flex.flex-column.flex-item-fluid.flex-nowrap.reset4print > div > div > div > header > div.flex.flex-justify-end.topnav-container.on-mobile-no-flex.flex-item-centered-vert.no-print > ul > li.topnav-listItem.relative.no-mobile > button")).Click();
            driver.FindElement(By.CssSelector("button[class='button button-solid-norm w100")).Click();

        }

        /// <summary>
        /// Tests the login process with white space in login and password fields.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Attempts to log in with white space in both login and password fields.
        /// 2. Verifies the presence of an error message for the login field.
        /// </remarks>
        [Test]
        public void VerifyEmailWIthEmptyFields()

        {
            LogInWithCredentials("", "");
            IWebElement errorMessageNoLogInName = driver.FindElement(By.XPath("//*[@id=\"id-3\"]/span"));
            IWebElement errorMessageNoLogInPassword = driver.FindElement(By.XPath("//*[@id=\"id-4\"]/span"));
            Assert.IsTrue(errorMessageNoLogInName.Displayed && errorMessageNoLogInPassword.Displayed);
        }

        /// <summary>
        /// Tests the login process with an invalid login.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Attempts to log in with an invalid login and password.
        /// 2. Verifies the presence of an error message.
        /// </remarks>
        [Test]
        public void VerifyEmailWIthInvalidLogin()

        {
            LogInWithCredentials("$!@", "fabs");
            IWebElement errorMessage = driver.FindElement(By.ClassName("notification__content"));
            Assert.IsTrue(errorMessage.Displayed);
        }

        [Test]
        public void VerifyEmailWIthWhiteSpace()

        {
            LogInWithCredentials(" ", " ");
            IWebElement errorMessageNoLogInName = driver.FindElement(By.XPath("//*[@id=\"id-3\"]/span"));
            Assert.IsTrue(errorMessageNoLogInName.Displayed);
        }

        private void LogInWithCredentials(string login, string passwordAccount)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            driver.Url = testUrl;

            IWebElement signInElement = driver.FindElement(By.CssSelector(
                "#gatsby-focus-wrapper > div.fixed.top-0.z-30.w-full.duration-75.ease-linear.bg-purple-25 > div > div > header > div > div.ml-auto.hidden.items-center.xl\\:shrink-0.md\\:flex.md\\:gap-4 > a.flex.flex-row.items-center.text-center.font-bold.rounded-full.group.transition-all.duration-300.no-underline.justify-center.items-center.transition-all.px-3.py-1\\.5.text-sm.text-purple-500.hover\\:\\!bg-purple-500.hover\\:\\!text-white.button-hover-shadow.focus\\:bg-purple-500.focus\\:text-white.w-fit > span > span"));

            signInElement.Click();

            IWebElement emailElement =
                driver.FindElement(By.CssSelector("#username"));
            emailElement.SendKeys(login);
            IWebElement password = driver.FindElement(By.XPath("//*[@id=\"password\"]"));
            password.SendKeys(passwordAccount);
            IWebElement loginBtn =
                driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div[1]/main/div[1]/div[2]/form/button"));
            loginBtn.Click();

            
        }


        [OneTimeTearDown]

        public void TearDown()

        {

            driver.Quit();

        }

    }

}
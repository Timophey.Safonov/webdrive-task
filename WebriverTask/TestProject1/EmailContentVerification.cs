﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using WebDriverTask.HelpClass;

namespace SeleniumCsharp

{

    public class EmailContent

    {
        String testUrl = "https://proton.me/";
        IWebDriver driver;


        [OneTimeSetUp]

        public void Setup()

        {
            driver = new ChromeDriver(@"D:\Studing\SELF-EDUCATION\AQA EPAM\C#\Test Automation\webdrive-task\WebriverTask\TestProject1\driver\chromedriver-win64\chromedriver-win64");

            driver.Url = "https://proton.me/";
        }

        /// <summary>
        /// Test for sending an email.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Maximizes the window and sets implicit wait timeout.
        /// 2. Navigates to the specified test URL.
        /// 3. Generates random email content and subject.
        /// 4. Logs in.
        /// 5. Composes and sends an email.
        /// 6. Logs out.
        /// 7. Verifies the presence of the Proton logo.
        /// </remarks>
        [Test, Order(1)]
        public void SendLetter()

        {
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            driver.Url = testUrl;


            string randomContent = LetterContentGenerator.GeneratorRandomEmailContent(300);
            string randomSubjectOfLetter = LetterContentGenerator.GeneratorRandomEmailContent(6);

            RandomText = randomContent;

            IWebElement signInElement = driver.FindElement(By.CssSelector("#gatsby-focus-wrapper > div.fixed.top-0.z-30.w-full.duration-75.ease-linear.bg-purple-25 > div > div > header > div > div.ml-auto.hidden.items-center.xl\\:shrink-0.md\\:flex.md\\:gap-4 > a.flex.flex-row.items-center.text-center.font-bold.rounded-full.group.transition-all.duration-300.no-underline.justify-center.items-center.transition-all.px-3.py-1\\.5.text-sm.text-purple-500.hover\\:\\!bg-purple-500.hover\\:\\!text-white.button-hover-shadow.focus\\:bg-purple-500.focus\\:text-white.w-fit > span > span"));

            signInElement.Click();

            LogIn("testWebDriver1@proton.me", "1111Qwer");


            IWebElement newMail = driver.FindElement(By.CssSelector(
                "body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div.content.ui-prominent.flex-item-fluid-auto.flex.flex-column.flex-nowrap.reset4print > div > div.sidebar.flex.flex-nowrap.flex-column.no-print.outline-none > div.px-3.pb-2.flex-item-noshrink.no-mobile > button"));

            newMail.Click();

            IWebElement emailAddress = driver.FindElement(By.XPath("//input[@placeholder='Email address']"));

            emailAddress.SendKeys("testEmailWebDriverproton1.me@proton.me");
            IWebElement subject = driver.FindElement(By.XPath("//input[@placeholder='Subject']"));
            subject.SendKeys(randomSubjectOfLetter);


            IWebElement iframeElement = driver.FindElement(By.XPath("//iframe[@title='Email composer']"));

            driver.SwitchTo().Frame(iframeElement);

            IWebElement textareaElement = driver.FindElement(By.CssSelector("#rooster-editor > div:nth-child(2)")); 
            textareaElement.SendKeys(randomContent);
            driver.SwitchTo().DefaultContent();
            

            IWebElement sendBtn = driver.FindElement(By.CssSelector("body > div.app-root > div:nth-child(5) > div > div > div > footer > div > div.button-group.button-group-solid-norm.button-group-medium > button.button.button-group-item.button-solid-norm.composer-send-button"));
            sendBtn.Click();
            Thread.Sleep(5000);

            LogOut();

            IWebElement logoProton = driver.FindElement(By.CssSelector("body > div.app-root > div.flex-no-min-children.flex-nowrap.flex-column.h100.scroll-if-needed.relative.sign-layout-bg > header > div.inline-flex.flex-nowrap.flex-item-noshrink"));
            Assert.IsTrue(logoProton.Displayed);


        }



        /// <summary>
        /// Test for retrieving and verifying an email.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Sleeps for a specified time to allow the email to be received.
        /// 2. Logs in with the recipient's credentials.
        /// 3. Finds and opens an unread email.
        /// 4. Retrieves the email content and sender.
        /// 5. Asserts the email content and sender.
        /// </remarks>
        [Test, Order(2)]
        public void GetLetter()

        {

            Thread.Sleep(60000);

            LogIn("testEmailWebDriverproton1.me@proton.me", "J3kCetx!6gvrdB:");
            // Find unread letter
            driver.FindElement(By.CssSelector(".button.button-small.button-ghost-weak.toolbar-button.toolbar-button--small.text-sm.m-0[aria-busy='false'][data-testid='filter-dropdown:show-unread']"))
                .Click();
            driver.FindElement(By.CssSelector(".flex-item-fluid.flex.flex-nowrap.cursor-pointer.opacity-on-hover-container.item-container-row.flex-align-items-center.unread")).Click();

            IWebElement iframeElement = driver.FindElement(By.XPath("//iframe[@title='Email content']"));

            driver.SwitchTo().Frame(iframeElement);
            IWebElement textareaElement = driver.FindElement(By.CssSelector("div[style*='width: 100% !important;padding-bottom:10px;!important'] > div:nth-child(2)"));
            string bodyEmail = textareaElement.Text;
            driver.SwitchTo().DefaultContent();
            
            IWebElement recipientName =
                driver.FindElement(By.ClassName("message-recipient-item-label"));
            string sender = recipientName.Text;
            Assert.IsTrue(bodyEmail.Contains(RandomText) && sender.Contains("testWebDriver1"));

        }

        /// <summary>
        /// Test for replying to an email.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Generates a nickname.
        /// 2. Logs in with the sender's credentials.
        /// 3. Opens the email for replying.
        /// 4. Composes and sends a reply with the generated nickname.
        /// 5. Logs out.
        /// </remarks>
        [Test, Order(3)]
        public void Reply()
        {
            string nickName = LetterContentGenerator.GeneratorNickName(6);
            RandomNickName = nickName;
            driver.FindElement(By.CssSelector("body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div > div > div.flex.flex-column.flex-item-fluid.flex-nowrap.reset4print > div > div > div > main > div > div > section > div > div.scroll-inner > div > div > div > article > div.message-header.px-5.message-header-expanded.is-inbound.message-header-expanded--without-details > div.pt-0.flex.flex-justify-space-between > div.button-group.button-group-outline-weak.button-group-medium.mb-2 > button:nth-child(1)")).Click();
            IWebElement iframeElement = driver.FindElement(By.XPath("//iframe[@title='Email composer']"));

            driver.SwitchTo().Frame(iframeElement);
            IWebElement textareaElement = driver.FindElement(By.CssSelector("#rooster-editor > div:first-child"));

            textareaElement.SendKeys(nickName);
            driver.SwitchTo().DefaultContent();
            
            driver.FindElement(By.CssSelector("body > div.app-root > div:nth-child(5) > div > div > div > footer > div > div.button-group.button-group-solid-norm.button-group-medium > button.button.button-group-item.button-solid-norm.composer-send-button")).Click();
            Thread.Sleep(7000);
            LogOut();
            IWebElement logoProton = driver.FindElement(By.CssSelector("body > div.app-root > div.flex-no-min-children.flex-nowrap.flex-column.h100.scroll-if-needed.relative.sign-layout-bg > header > div.inline-flex.flex-nowrap.flex-item-noshrink"));
            Assert.IsTrue(logoProton.Displayed);
        }


        /// <summary>
        /// Test for changing the nickname.
        /// </summary>
        /// <remarks>
        /// This test case performs the following steps:
        /// 1. Sleeps to allow time for an email to be received.
        /// 2. Logs in with the sender's credentials.
        /// 3. Retrieves an email and extracts its body.
        /// 4. Opens the settings and navigates to account settings.
        /// 5. Edits the display name.
        /// 6. Saves the changes and waits for confirmation.
        /// 7. Asserts the new nickname.
        /// </remarks>
        [Test, Order(4)]
        public void ChangeNickName()
        {
            
            Thread.Sleep(90000);

            LogIn("testWebDriver1@proton.me", "1111Qwer");

            // This can be also a seperate method
            driver.FindElement(By.CssSelector(".button.button-small.button-ghost-weak.toolbar-button.toolbar-button--small.text-sm.m-0[aria-busy='false'][data-testid='filter-dropdown:show-unread']"))
                .Click();
            driver.FindElement(By.CssSelector(".flex-item-fluid.flex.flex-nowrap.cursor-pointer.opacity-on-hover-container.item-container-row.flex-align-items-center.unread")).Click();

            IWebElement iframeElement = driver.FindElement(By.XPath("//iframe[@title='Email content']"));

            driver.SwitchTo().Frame(iframeElement);
            IWebElement textareaElement = driver.FindElement(By.CssSelector("div[style*='width: 100% !important;padding-bottom:10px;!important'] > div:nth-child(1)"));
            string bodyEmail = textareaElement.Text;
            driver.SwitchTo().DefaultContent();

            IWebElement settings = driver.FindElement(By.XPath(
                "//button[@class='drawer-sidebar-button rounded flex interactive no-pointer-events-children relative drawer-sidebar-button--notification']"));
            settings.Click();

            IWebElement allSettings = driver.FindElement(By.XPath("//a[normalize-space()='All settings']"));
            allSettings.Click();

            IWebElement accountAndPassword =
                driver.FindElement(By.CssSelector("body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div > div > div.sidebar.flex.flex-nowrap.flex-column.no-print.outline-none > div.flex-item-fluid.flex-nowrap.flex.flex-column.overflow-overlay.pb-4.md\\:mt-2 > nav > ul > ul:nth-child(1) > li:nth-child(5) > a"));
            accountAndPassword.Click();

            IWebElement editName = driver.FindElement(By.XPath("//button[normalize-space()='Edit']"));
            editName.Click();

            IWebElement displayName = driver.FindElement(By.XPath("//input[@id='displayName']"));
            displayName.SendKeys(bodyEmail);

            IWebElement saveBTN =
                driver.FindElement(By.CssSelector("button.button.button-solid-norm[aria-busy='false'][type='submit']"));
            saveBTN.Click();

            Thread.Sleep(5000);

            IWebElement changedName = driver.FindElement(By.CssSelector("div.text-ellipsis.user-select"));

            string newNick = changedName.Text;

            Assert.IsTrue(newNick.Contains(bodyEmail));

        }




        [OneTimeTearDown]

        public void TearDown()

        {

            driver.Quit();

        }


        public string? RandomText { get; set; }

        public string? RandomNickName { get; set; }


        private void LogIn(string username, string password)
        {
            IWebElement emailElement =
                driver.FindElement(By.CssSelector("#username"));
            emailElement.SendKeys(username);
            IWebElement passwordAccount = driver.FindElement(By.XPath("//*[@id=\"password\"]"));
            passwordAccount.SendKeys(password);
            IWebElement loginBtn =
                driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div[1]/main/div[1]/div[2]/form/button"));
            loginBtn.Click();
        }

        private void LogOut()
        {
            driver.FindElement(By.CssSelector(
                    "body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div > div > div.flex.flex-column.flex-item-fluid.flex-nowrap.reset4print > div > div > div > header > div.flex.flex-justify-end.topnav-container.on-mobile-no-flex.flex-item-centered-vert.no-print > ul > li.topnav-listItem.relative.no-mobile > button"))
                .Click();
            driver.FindElement(By.CssSelector("button[class='button button-solid-norm w100")).Click();
        }
    }

}